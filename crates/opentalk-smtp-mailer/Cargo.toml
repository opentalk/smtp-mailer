# SPDX-FileCopyrightText: OpenTalk GmbH <mail@opentalk.eu>
#
# SPDX-License-Identifier: EUPL-1.2

[package]
build = "build.rs"
default-run = "opentalk-smtp-mailer"
description = "Mail Worker that takes mail send tasks from a queue, renders them, and sends them out via SMTP"
edition = "2021"
name = "opentalk-smtp-mailer"
publish = false
version.workspace = true

[dependencies]
anyhow.workspace = true
async-trait.workspace = true
bytes.workspace = true
chrono = { workspace = true, features = ["serde"] }
chrono-tz.workspace = true
clap = { workspace = true, features = ["derive"] }
config = { workspace = true, default-features = false, features = ["toml"] }
env_logger.workspace = true
futures.workspace = true
ics-chrono-tz.workspace = true
lapin = { workspace = true, default-features = false }
log.workspace = true
opentalk-mail-worker-protocol.workspace = true
opentalk-types-common = { workspace = true, features = ["serde"] }
serde = { workspace = true, features = ["derive"] }
serde_json.workspace = true
serde_path_to_error.workspace = true
thiserror.workspace = true
tokio = { workspace = true, default-features = false, features = [
  "macros",
  "rt-multi-thread",
  "signal",
] }
tokio-executor-trait.workspace = true
tokio-reactor-trait.workspace = true
url = { workspace = true, features = ["serde"] }
uuid = { workspace = true, features = ["serde", "v4"] }

fluent-templates = { workspace = true, features = ["tera"] }
percent-encoding.workspace = true
phonenumber.workspace = true
tera = { workspace = true, features = ["builtins"] }

css-inline.workspace = true
ics.workspace = true
lettre = { workspace = true, default-features = false, features = [
  "builder",
  "hostname",
  "smtp-transport",
  "tokio1",
  "tokio1-rustls-tls",
] }
opentalk-version.workspace = true
service-probe.workspace = true
textwrap.workspace = true

[dev-dependencies]
insta.workspace = true
pretty_assertions.workspace = true
rstest.workspace = true

[build-dependencies]
opentalk-version.workspace = true
snafu.workspace = true
vergen = { workspace = true, features = ["build", "cargo", "rustc"] }
vergen-gix.workspace = true
